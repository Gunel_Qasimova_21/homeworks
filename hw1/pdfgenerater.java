package com.itextpdf.text.pdf;
	import com.itextpdf.text.Document;
	import com.itextpdf.text.Paragraph;
	import com.itextpdf.text.pdf.PdfWriter;
	public class pdf {
	 public static void main(String arg[])throws Exception 
        {
		 Document document=new Document(); 
		  PdfWriter.getInstance(document, new FileOutputStream("PrivateData.pdf"));
		  document.open();
		  document.add(new Paragraph("A Private Data PDF document."));
	         document.close();
	}
	  private static void addMetaData(Document document) {
          document.addTitle("My first PDF document");
          document.addSubject("Using iText");
          document.addKeywords("Java, PDF, iText");
          document.addAuthor("Gunel");
          document.addCreator("Gunel");
	  }
	}